# G2A Data viewer
Выгрузка данных о ценах, комиссиях, выгоде и прочим данным продаж на цифровом портале G2A.
### Основные задачи

  - Вывод данных о выплатах
  - Управление данными
### ТЗ
ТЗ заказчика вынесено в отдельный файл ***TZ.DOC***
### Используемые пакеты

* [Laravel-7](http://laravel.com) - Base framework
* [Currency Converter](https://github.com/florianv/laravel-swap) - Laravel currency converter
