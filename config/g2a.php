<?php


return [
    'test' => [
        'import' => [
            'url' => 'https://sandboxapi.g2a.com/v2/',
            'email' => 'sandboxapitest@g2a.com',
            'client_id' => 'ibHtsEljmCxjOFAn', 
            'client_secret' => 'HrsPmuOlWjqBMHnQWIgfchUqBTBYcRph', 
            'redirect' => 'whatisthis?',
            'token_url' => 'https://sandboxapi.g2a.com/oauth/token',
        ],
        'export' => [
            'url' => 'https://sandboxapi.g2a.com/v1/',
            'email' => 'sandboxapitest@g2a.com',
            'client_id' => 'qdaiciDiyMaTjxMt',
            'client_secret' => 'b0d293f6-e1d2-4629-8264-fd63b5af3207b0d293f6-e1d2-4629-8264-fd63b5af3207', 
        ],
    ],
    'production' => [
        'import' => [
            'url' => 'https://api.g2a.com/v2/',
            'email' => '1',
            'client_id' => '2',
            'client_secret' => '3',
            'redirect' => 'whatisthis?',
            'token_url' => 'https://api.g2a.com/oauth/token',
        ],
        'export' => [
            'url' => 'https://api.g2a.com/v1/',
            'email' => '',
            'client_id' => '',
            'client_secret' => '', 
        ],
    ]
];