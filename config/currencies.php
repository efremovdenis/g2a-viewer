<?php

return [
    'USD' => ['visible' => true, 'name' => 'USD', 'symbol' => '$', 'btn-bg' => 'outline-success'],
    'EUR' => ['visible' => true, 'name' => 'EUR', 'symbol' => '€', 'btn-bg' => 'outline-secondary'],
    'GBP' => ['visible' => true, 'name' => 'GPB', 'symbol' => '£', 'btn-bg' => 'outline-info'],
    'RUB' => ['visible' => true, 'name' => 'RUB', 'symbol' => '₽', 'btn-bg' => 'outline-primary'],
];