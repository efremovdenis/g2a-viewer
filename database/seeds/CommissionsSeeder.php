<?php

use Illuminate\Database\Seeder;
use App\Commission;

class CommissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Commission::truncate();
        $data = [];
        $data[] = ['min_limit' => 0, 'max_limit' => 1.99, 'percent' => 10];
        $data[] = ['min_limit' => 2, 'max_limit' => 4.99, 'percent' => 20];
        $data[] = ['min_limit' => 5, 'max_limit' => null, 'percent' => 34];

        foreach($data as $row)
            Commission::create($row);
    }
}
