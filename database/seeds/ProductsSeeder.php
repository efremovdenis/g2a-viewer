<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        $data = [];
        $data[] = ['name' => "No Man's Sky Steam Key GLOBAL", 'price' => 21.16, 'g2a_product_id' => '10000016743015', 'commission' => 0.6, 'platform' => 'steam', 'publisher' => 'key', 'amount' => 17, 'is_needed' => true];
        $data[] = ['name' => "Overwatch: Game of the Year Edition Blizzard Key GLOBAL", 'price' => 100, 'g2a_product_id' => '10000042132001', 'commission' => 0.5, 'platform' => 'steam', 'publisher' => 'gift', 'amount' => 13, 'is_fixed' => true, 'is_index' => true];
        $data[] = ['name' => "The Sims 3 Origin Key GLOBAL", 'price' => 25.15, 'g2a_product_id' => '10000023727004', 'commission' => 0.5, 'platform' => 'windows', 'publisher' => 'gift', 'amount' => 20, 'is_fixed' => false, 'is_index' => false];
        $data[] = ['name' => "Dark Souls III Steam Key GLOBAL", 'price' => 16.43, 'g2a_product_id' => '10000002685013', 'commission' => 0.3, 'platform' => 'steam', 'publisher' => 'key', 'amount' => 5, 'is_fixed' => false, 'is_index' => false];
        $data[] = ['name' => "F1 2017 Steam Key GLOBAL", 'price' => 12.22, 'g2a_product_id' => '10000041761002', 'commission' => 0.5, 'platform' => 'steam', 'publisher' => 'key', 'amount' => 2, 'is_fixed' => false, 'is_index' => true];
        

        foreach($data as $row)
            Product::create($row);
    }
}
