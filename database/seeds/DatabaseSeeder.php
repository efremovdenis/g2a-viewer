<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        factory(App\User::class, 10)->create();
        App\User::create(['name' => 'root', 'email' => 'root', 'password' => \Hash::make('password')]);
        App\User::create(['name' => 'Test G2A account', 'email' => 'sandboxapitest@g2a.com', 'password' => \Hash::make('password'), 'public_key' => 'ibHtsEljmCxjOFAn', 'secret_key' => 'HrsPmuOlWjqBMHnQWIgfchUqBTBYcRph']);

        $this->call('ProductsSeeder');
        $this->call('CommissionsSeeder');
        $this->call('SettingsSeeder');
    }
}
