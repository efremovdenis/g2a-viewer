<?php

use Illuminate\Database\Seeder;

use App\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create(['code' => 'is_fixed_message', 'value' => '']);
        Setting::create(['code' => 'product_not_found', 'value' => 'Данный товар отсутствует в списке. За подробностями напишите по контактам.']);
    }
}
