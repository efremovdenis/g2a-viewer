<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('g2a_product_id')->primary();
            $table->string('name');
            $table->float('price');
            $table->float('commission');
            $table->string('platform');
            $table->string('publisher');
            $table->boolean('is_fixed')->default(false);
            $table->boolean('is_index')->default(false);
            $table->boolean('is_needed')->default(false);
            $table->boolean('is_not_needed')->default(false);
            $table->boolean('is_not_needed_force')->default(false);
            $table->integer('amount')->default(1);
            $table->integer('quantity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
