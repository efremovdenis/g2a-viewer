<?php
namespace App\Services;

use GuzzleHttp\Client;
use Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class G2AExport {
    private function getHeaders() {
        $headers['Authorization'] = config('g2a.'.env('G2A_ENV', 'test').'.export.client_id').', '.$this->getApiKey();
        $headers['Content-Type'] = "application/json";
        //dd($headers);
        return $headers;
    }
    private function getApiKey() {
        return hash('sha256', config('g2a.'.env('G2A_ENV', 'test').'.export.client_id') . config('g2a.'.env('G2A_ENV', 'test').'.export.email') . config('g2a.'.env('G2A_ENV', 'test').'.export.client_secret'));
        if(!Auth::check()) return "";
        return hash('sha256',  Auth::user()->public_key . Auth::user()->email . Auth::user()->secret_key);
    }
    private function getClient() {
        return new Client([
            'base_uri' => config('g2a.'.env('G2A_ENV', 'test').'.export.url'), 
            'headers' => $this->getHeaders()
        ]);
    }
    private function getResponse($method, $uri, $query = []) {
        $response = $this->getClient()->request(mb_strtoupper($method), $uri, ['query' => $query]);
        $content = $response->getBody()->getContents();
        if(env('G2A_LOG_EXPORT', true)) {
            Log::channel('g2a_export')->warning('SEND REQUEST TO '.config('g2a.'.env('G2A_ENV', 'test').'.export.url').$uri);
            Log::channel('g2a_export')->warning('QUERY');
            Log::channel('g2a_export')->warning(print_r($query, true));
            Log::channel('g2a_export')->warning('RESPONSE CODE '.print_r($response->getStatusCode(), true));
            Log::channel('g2a_export')->warning('RESPONSE BODY');
            Log::channel('g2a_export')->warning(print_r(json_decode($response->getBody(), true), true));
        }
        return json_decode($content, true);
    }

    public function getProducts($page = 1) {
        return $this->getResponse('get', 'products', ['page' => $page]);
    }
    public function getProduct($id) {
        return $this->getResponse('get', 'products', ['id' => $id]);
    }
    public function getAllProducts() {
        $page = 1;
        $products = [];
        do {
            $result = $this->getProducts($page++)['docs'];
            $products = array_merge($products, $result);
        } while (count($result) > 0);
        return $products;
    }
}