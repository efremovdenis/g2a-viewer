<?php
namespace App\Services;


use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

use GuzzleHttp\ClientInterface;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;

class G2AImport extends AbstractProvider implements ProviderInterface
{
    private $token = null;
    private $response = null;
    
    public function setCredentials($clientId, $clientSecret) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        return $this;
    }
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://localhost/LOGIN', $state);
    }
    protected function getTokenUrl()
    {
        return config('g2a.'.env('G2A_ENV', 'test').'.import.token_url');
    }
    protected function getTokenFields($code)
    {
        return Arr::add(
            parent::getTokenFields($code), 'grant_type', 'client_credentials'
        );
    }
    protected function formatScopes(array $scopes, $scopeSeparator)
    {
        return implode($scopeSeparator, $scopes);
    }
    protected function mapUserToObject(array $user)
    {
        return collect([
            'balance'     => $user['balance'],
            'available_balance'     => $user['available_balance'],
        ]);
    }
    public function getAccessToken($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)],
            'body'    => $this->getTokenFields($code),
            'http_errors' => false,
        ]);
        
        return $this->parseAccessToken($response->getBody());
    }
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get(config('g2a.'.env('G2A_ENV', 'test').'.import.url').'users/me/balance', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
            'http_errors' => false,
        ]);

        return json_decode($response->getBody(), true);
    }
    public function getAccessTokenResponse($code)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Accept' => 'application/json'],
            'http_errors' => false,
            $postKey => $this->getTokenFields($code),
        ]);

        return json_decode($response->getBody(), true);
    }
    private function getToken() {
        
        if(is_null($this->token)) {
            $response = $this->getAccessTokenResponse($this->getCode());
            $token = Arr::get($response, 'access_token');
            $this->token = $token;
        }
        return $this->token;
    }
    protected function getResponse($url, $method = 'get', $data = [], $headers = []) {
        $query = array_key_exists('query', $data) ? $data['query'] : null;
        $body = array_key_exists('body', $data) ? $data['body'] : null;
        $headers['Authorization'] = "Bearer ". $this->getToken();
        
        $response = $this->getHttpClient()->$method(config('g2a.'.env('G2A_ENV', 'test').'.import.url').$url, [
            'http_errors' => false,
            'headers' => $headers,
            'query' => $query,
            'body' => !is_null($body) ? json_encode($body) : null
        ]);

        if(env('G2A_LOG_IMPORT', true)) {
            Log::channel('g2a_import')->warning('SEND REQUEST TO '.config('g2a.'.env('G2A_ENV', 'test').'.import.url').$url);
            Log::channel('g2a_import')->warning('HEADERS');
            Log::channel('g2a_import')->warning(print_r($headers, true));
            Log::channel('g2a_import')->warning('QUERY');
            Log::channel('g2a_import')->warning(print_r($query, true));
            Log::channel('g2a_import')->warning('BODY');
            Log::channel('g2a_import')->warning(print_r(json_encode($body), true));
            Log::channel('g2a_import')->warning('RESPONSE CODE '.print_r($response->getStatusCode(), true));
            Log::channel('g2a_import')->warning('RESPONSE BODY');
            Log::channel('g2a_import')->warning(print_r(json_decode($response->getBody(), true), true));
        }

        
        return collect(['code' => $response->getStatusCode(), 'body' => json_decode($response->getBody(), true)]);
    }
    /* Auctions */
    public function getAuctions($page = 1) {
        $response = $this->getResponse('auctions', 'get', ['query' => ['page' => $page]]);
        return $response;
    }
    public function getAllAuctions() {
        $auctions = [];
        $page = 1;
        do {
            $response = $this->getAuctions($page++);
            
            $dAuctions = $response->get('body')['items'];
            $auctions = array_merge($auctions, $dAuctions);
        } while(count($dAuctions) > 0);
        return $auctions;
    }
    public function storeAuction($auction) {
        $data = [
            'product_id' => (int) $auction->product->g2a_product_id,
            'type' => 'dropshipping',
            'price' => (float) $auction->price,
            'inventory_type' => $auction->inventory_type,
            'inventory_size' => (int) $auction->inventory()->where('status', 'active')->count(),
            'status' => $auction->status,
            'visibility' => $auction->visibility
        ];
        if(in_array($auction->visibility, ['all', 'business'])) {
            $data['business_price'] = (float) $auction->business_price;
        }
        return $this->getResponse('auctions', 'post', ['body' => $data], ['Content-Type' => 'application/json']);
    }
    public function updateAuction($auction) {
        $data = [
            'price' => (float) $auction->price,
            'inventory_type' => $auction->inventory_type,
            'inventory_size' => (int) $auction->inventory()->where('status', 'active')->count(),
            'status' => $auction->status,
            'visibility' => $auction->visibility
        ];
        if(in_array($auction->visibility, ['all', 'business'])) {
            $data['business_price'] = (float) $auction->business_price;
        }
        return $this->getResponse('auctions/'.$auction->g2a_auction_id, 'patch', ['body' => $data], ['Content-Type' => 'application/json']);
    }
    public function getAuction() {
        
    }



    public function getBalance() {

    }
}