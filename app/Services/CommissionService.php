<?php
namespace App\Services;

use App\Commission;

class CommissionService {
    private static $instanse = null;
    private $commissions = null;
    private function __construct() {
        $this->commissions = Commission::query()->orderBy('min_limit', 'desc')->get();
    }
    public static function getInstance() {
        if(is_null(self::$instanse)) {
            self::$instanse = new self();
        }        
        return self::$instanse;
    }
    public static function commission($price) {
        $commission = self::getInstance()->commissions->where('min_limit', '<', $price)->first();
        return $commission;
    }
}