<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\CommissionRequest;

use App\User;
use App\Setting;
use App\Product;
use App\Commission;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    protected $user;
    
    public function login() {
        return view('admin.login');
    }
    public function auth(Request $request) {
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)) {
            return redirect()->to(route('admin.index'));
        } else {
            return redirect()->back()->with('danger', 'User not found!');
        }
    }
    public function index() {
        $users = User::all();
        $commissions = Commission::orderBy('min_limit')->get();
        $products = Product::all();

        return view('admin.index', compact('users', 'commissions', 'products'));
    }

    public function commissionStore(CommissionRequest $request) {
        $commission = new Commission($request->validated());
        if($commission->min_limit >= $commission->max_limit) {
            return redirect()->to(route('admin.index'))->with('danger', 'Min limit must be less than max limit');
        }
        //dd($commission->min_limit.  '<' .Commission::where('max_limit', '<', $commission->min_limit)->first()->max_limit);
        // dd(Commission::where('max_limit', '>', $commission->min_limit)->first());
        if(!is_null(Commission::where('max_limit', '>', $commission->min_limit)->first())) {
            return redirect()->to(route('admin.index'))->with('danger', 'Typed min limit less than existed max limit.');
        }

        $commission->save();
        if($commission->exists) {
            return redirect()->to(route('admin.index'))->with('success', 'Commission stored');
        } else {
            return redirect()->to(route('admin.index'))->with('danger', 'Commission not stored');
        }
    }
    public function commissionDelete($id) {
        $commission = Commission::find($id);
        if(is_null($commission)) {
            return redirect()->back()->with('danger', 'Commission not found');
        }        
        $result = $commission->delete();
        if($result) return redirect()->back()->with('success', 'Commission deleted');
        else return redirect()->back()->with('danger', 'Commission not deleted');
    }
    public function commissionDeleteAll() {
        $result = Commission::truncate();
        if($result) return redirect()->back()->with('success', 'All commissions deleted');
        else return redirect()->back()->with('danger', 'All commissions not deleted');
    }

    public function userStore(UserRequest $request) {
        $user = new User($request->validated());
        $user->password = Hash::make($user->password);
        $result = $user->save();
        if($result) {
            return redirect()->to(route('admin.index'))->with('success', 'User stored');
        } else {
            return redirect()->to(route('admin.index'))->with('danger', 'User not stored');
        }
    }
    public function userDelete($id) {
        $user = User::find($id);
        if(is_null($user)) {
            return redirect()->back()->with('danger', 'User not found');
        }
        if(Auth::user() == $user) {
            return redirect()->back()->with('danger', 'You can not delete own account');
        }
        $result = $user->delete();
        if($result) return redirect()->back()->with('success', 'User deleted');
        else return redirect()->back()->with('danger', 'User not deleted');
    }

    public function productUnfix($g2a_id) {
        $product = Product::find($g2a_id);
        if(is_null($product)) {
            return redirect()->back()->with('danger', 'Product not found');
        }
        $result = $product->update(['is_fixed' => false]);
        if($result) {
            return redirect()->back()->with('success', 'Product unfixed');
        } else {
            return redirect()->back()->with('danger', 'Product not unfixed');
        }
    }
    public function productUnfixAll() {
        $result = Product::where('is_fixed', true)->update(['is_fixed' => false]);
        if($result) {
            return redirect()->back()->with('success', 'All products unfixed');
        } else {
            return redirect()->back()->with('danger', 'Nothing was unfixed');
        }
    }
    public function productsFix(Request $request) {
        $ids = collect(explode("\r\n", $request->get('ids')))->unique();
        $products = Product::whereIn('g2a_product_id', $ids->all());
        $result = $products->update(['is_fixed' => true]);
        if($result) {
            return redirect()->back()->with('success', 'Requested products fixed');
        } else {
            return redirect()->back()->with('danger', 'Nothing was fixed');
        }
    }
    public function productsFixMessage(Request $request) {
        $result = Setting::find('is_fixed_message')->update(['value' => $request->get('message')]);
        if($result) {
            return redirect()->back()->with('success', 'Message updated');
        } else {
            return redirect()->back()->with('danger', 'Message not updated');
        }
    }

    public function productUnneed($g2a_id) {
        $product = Product::find($g2a_id);
        if(is_null($product)) {
            return redirect()->back()->with('danger', 'Product not found');
        }
        $result = $product->update(['is_needed' => false]);
        if($result) {
            return redirect()->back()->with('success', 'Product unneded');
        } else {
            return redirect()->back()->with('danger', 'Product not unneded');
        }
    }
    public function productUnneedAll() {
        $result = Product::where('is_needed', true)->update(['is_needed' => false]);
        if($result) {
            return redirect()->back()->with('success', 'All products unfixed');
        } else {
            return redirect()->back()->with('danger', 'Nothing was unfixed');
        }
    }

    public function productsUnunneedAll() {
        $result = Product::where('is_not_needed_force', true)->update(['is_not_needed_force' => false, 'is_not_needed' => false]);
        if($result) {
            return redirect()->back()->with('success', 'All products FORCE unneeded');
        } else {
            return redirect()->back()->with('danger', 'Nothing was changed');
        }
    }
    public function productUnunneed($g2a_id) {
        $product = Product::find($g2a_id);
        if(is_null($product)) {
            return redirect()->back()->with('danger', 'Product not found');
        }
        $result = $product->update(['is_not_needed' => false, 'is_not_needed_force' => false]);
        if($result) {
            return redirect()->back()->with('success', 'Product FORCE ununneded');
        } else {
            return redirect()->back()->with('danger', 'Product not changed');
        }
    }
    public function productsUnneed(Request $request) {
        $ids = collect(explode("\r\n", $request->get('ids')))->unique();
        $products = Product::whereIn('g2a_product_id', $ids->all());
        
        $result = $products->update(['is_not_needed_force' => true, 'is_not_needed' => true]);
        if($result) {
            return redirect()->back()->with('success', 'Requested products set as FORCE unneeded');
        } else {
            return redirect()->back()->with('danger', 'Nothing was changed');
        }
    }

    public function productsNeed(Request $request) {
        $ids = collect(explode("\r\n", $request->get('ids')))->unique();
        $products = Product::whereIn('g2a_product_id', $ids->all());
        $result = $products->update(['is_needed' => true]);
        if($result) {
            return redirect()->back()->with('success', 'Requested products set as needed');
        } else {
            return redirect()->back()->with('danger', 'Nothing was changed');
        }
    }
    public function productUnindex($g2a_id) {
        $product = Product::find($g2a_id);
        if(is_null($product)) {
            return redirect()->back()->with('danger', 'Product not found');
        }
        $result = $product->update(['is_index' => false]);
        if($result) {
            return redirect()->back()->with('success', 'Product stopped at Index');
        } else {
            return redirect()->back()->with('danger', 'Product not updated');
        }
    }
    public function productUnindexAll() {
        $result = Product::where('is_index', true)->update(['is_index' => false]);
        if($result) {
            return redirect()->back()->with('success', 'All products removed from index');
        } else {
            return redirect()->back()->with('danger', 'Nothing was changed');
        }
    }
    public function productsIndex(Request $request) {
        $ids = collect(explode("\r\n", $request->get('ids')))->unique();
        $products = Product::whereIn('g2a_product_id', $ids->all());
        $result = $products->update(['is_index' => true]);
        if($result) {
            return redirect()->back()->with('success', 'Requested products set to Index');
        } else {
            return redirect()->back()->with('danger', 'Nothing was changed');
        }
    }
}
