<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use Illuminate\Support\Facades\View;


class ProductsController extends Controller
{

    public function index(Request $request) {
        $product = null;
        
        if($request->has('link')) {
            $parts = explode("-", $request->get('link'));
            $id = mb_substr($parts[count($parts) - 1], 1);
            $product = Product::where('g2a_product_id', $id)->first();
            if(is_null($product)) {
                return redirect()->to(route('products.index'))->with('system', 'product_not_found');
            }
        }
        $products = Product::where('is_needed', true)->orWhere('is_index', true)->where('is_not_needed_force', false)->get();
        if($products->count() <= 0) {
            die("NULL PRODUCTS");
        }
        $neededProducts = $products->where('is_needed', '=', true);

        if(is_null($product)) {
            $indexProducts = $products->where('is_index', '=', true);        
            if($indexProducts->count() <= 0) {
                $indexProducts = $products;
            }        
            $product = $indexProducts->random();
        }
        

        return view('products.index', compact('neededProducts', 'product'));
    }

    public function gamelist() {
        $products = Product::where('is_not_needed', false)->get();
        $platforms = $products->pluck('platform')->unique();
        $publishers = $products->pluck('publisher')->unique();
        View::share('title', 'Gamelist');
        return view('products.gamelist', compact('products', 'platforms', 'publishers'));
    }

    public function blacklist() {
        $products = Product::where('is_not_needed_force', true)->orWhere('is_not_needed', true)->get();
        $platforms = $products->pluck('platform')->unique();
        $publishers = $products->pluck('publisher')->unique();
        View::share('title', 'Blacklist');
        return view('products.blacklist', compact('products', 'platforms', 'publishers'));
    }
}
