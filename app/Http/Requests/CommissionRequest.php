<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'min_limit' => 'required|numeric',
            'percent' => 'required|numeric'
        ];
        if($this->get('max_limit') != null) {
            $rules['max_limit'] = 'numeric';
        }
        return $rules;
    }
}
