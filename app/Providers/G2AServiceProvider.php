<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\G2AImport;
use App\Services\G2AExport;

use Laravel\Socialite\Facades\Socialite;

class G2AServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(G2AExport::class, function ($app) {
            return new G2AExport();
        });
        $this->app->bind('g2a_export', function ($app) {
            return new G2AExport();
        });
        $this->app->bind('g2a_import', function ($app) {
            return Socialite::with('g2a')->stateless();
        });
        $this->app->bind(G2AImport::class, function($app) {
            return Socialite::with('g2a')->stateless();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'g2a',
            function ($app) use ($socialite) {
                $config = $app['config']['services.g2a'];
                return $socialite->buildProvider(G2AImport::class, $config);
            }
        );
    }
}
