<?php
namespace App\Passport;

use Laravel\Passport\Client as OAuthClient; 

use App\Traits\UsesUuid;

class Client extends OAuthClient
{ 
    use UsesUuid;
    public $incrementing = false; 


}