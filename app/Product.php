<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Swap\Laravel\Facades\Swap;

use App\Services\CommissionService;
use App\Services\ProductCommissionService;

class Product extends Model
{
    protected $primaryKey = 'g2a_product_id';
    protected $table = 'products';
    protected $fillable = ['g2a_product_id', 'name', 'commission', 'price', 'is_fixed','is_needed', 'is_not_needed', 'is_index', 'amount', 'quantity', 'platform', 'publisher', 'is_not_needed_force'];
    public $timestamps = false;
    
    public function getQtyAttribute() {
        return $this->amount - $this->quantity;
    }
    public function getCommissionAttribute() {
        // return 1;
        if($this->is_fixed) return $this->attributes['commission'];
        $commission = ProductCommissionService::commission($this->price);
        //dd($commission);
        if(is_null($commission)) return $this->attributes['commission'];
        return $commission->percent;
    }

    public function price($type, $currency) {
        try {
            switch($currency) {
                case "USD":
                    $rate = 1;
                break;
                case "RUB":
                    $rate = Swap::latest('USD/RUB');
                    $rate = $rate->getValue();
                break;
                default:
                    $rate = Swap::latest('USD/RUB');
                    $rate = $rate->getValue();

                    $_rate = Swap::latest($currency.'/RUB');
                    $rate = $rate / ($_rate->getValue() == 0 ? 1 : $_rate->getValue());
                break;
            }
        } catch(\Throwable $e) {
            $rate = 1;
        }
        
        $commission = CommissionService::commission($this->price);
        if(is_null($commission)) $percent = 100;
        else $percent = $commission->percent;
        
        switch($type) {
            case "our_profit":
                $result = $this->price * $rate * (1 - $this->commission - (100 - $percent)/100);
            break;
            case "your_profit":
                $result = $this->price * $rate * $this->commission;
            break;
            case "g2a_price":
                $result = $this->price * $rate;
            break;
            case "g2a_commission":
                $result = $this->price * $rate * (100 - $percent)/100;                
            break;
            default:
                $result = $rate * $this->price;
            break;
        }
        return number_format($result, 2, ',', '');
    }
}
