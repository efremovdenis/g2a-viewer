<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\User;
use App\Product;
use App\Services\G2AImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(G2AImport $import) {
            $_products = Product::all();            
            $_users = User::g2a()->get();
            $updatings = [];
            if(env('G2A_LOG_CRON', true))
                Log::channel('cron')->info('CRON Start');
            foreach($_users as $user) {
                if(env('G2A_LOG_CRON', true))
                    Log::channel('cron')->info('Check user keys: '.$user->public_key . ' : '.$user->secret_key);
                if($user->public_key == "Keys are stored from another app." || $user->secret_key == "Keys are stored from another app.") continue;
                if(env('G2A_LOG_CRON', true))
                    Log::channel('cron')->info('Getting user auctions:');
                $auctions = collect($import->setCredentials($user->public_key, $user->secret_key)->getAllAuctions());
                if(env('G2A_LOG_CRON', true))
                    Log::channel('cron')->info($auctions);
                if(env('G2A_LOG_CRON', true))
                    Log::channel('cron')->info('Getting products:');
                $products = $_products->whereIn('g2a_product_id', $auctions->pluck('product_id'));
                foreach($products as $product) {
                    if(env('G2A_LOG_CRON', true))
                        Log::channel('cron')->info('Updating product#'.$product->g2a_product_id);      
                    if(!array_key_exists($product->g2a_product_id, $updatings)) $updatings[$product->g2a_product_id] = 0;
                    $updatings[$product->g2a_product_id] += $auctions->where('product_id', $product->g2a_product_id)->first()['inventory_size'];                               
                }                
            }
            DB::beginTransaction();
            if(env('G2A_LOG_CRON', true))
                Log::channel('cron')->info('Set all products quantity to zero and remove "not_needed" flag');
            DB::table('products')->update(['quantity' => 0, 'is_not_needed' => false]);
            if(env('G2A_LOG_CRON', true))
                Log::channel('cron')->info('MASS UPDATINGS FOR PRODUCTS START');
            foreach($updatings as $id => $qty) {
                DB::table('products')->where('g2a_product_id', $id)->update(['quantity' => $qty]);
            }
            if(env('G2A_LOG_CRON', true))
                Log::channel('cron')->info('After foreach updatings.');
            DB::table('products')->whereRaw('quantity >= amount')->update(['is_not_needed' => true]);
            DB::table('products')->where('is_not_needed_force', true)->update(['is_not_needed' => true]);
            DB::commit();
            if(env('G2A_LOG_CRON', true))
                Log::channel('cron')->info('CRON Done');            
        })->cron("* * * * *"); // * */30 * * *
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
