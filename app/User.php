<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Config;

use App\Services\G2AImport;
use Laravel\Passport\HasApiTokens;

use Illuminate\Contracts\Encryption\DecryptException;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'public_key', 'secret_key',
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public function scopeG2a($query) {
        return $query->where('public_key', '!=', null)->where('secret_key', '!=', null);
    }
    public function getPublicKeyAttribute($value) {
        try {
            $result = is_null($value) ? null : decrypt($value);
        } 
        catch(DecryptException $e) {
            $result = 'Keys are stored from another app.';
        }
        return $result;
    }
    public function setPublicKeyAttribute($value) {
        $this->attributes['public_key'] = encrypt($value);
    }
    public function getSecretKeyAttribute($value) {
        try {
            $result = is_null($value) ? null : decrypt($value);
        } 
        catch(DecryptException $e) {
            $result = 'Keys are stored from another app.';
        }
        return $result;
    }
    public function setSecretKeyAttribute($value) {
        $this->attributes['secret_key'] = encrypt($value);
    }

    public $timestamps = false;
}
