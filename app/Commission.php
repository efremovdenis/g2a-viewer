<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Commission extends Model
{
    use QueryCacheable;
    protected $cacheFor = 60*5;
    
    protected $table = 'commissions';
    protected $fillable = ['id', 'min_limit', 'max_limit', 'percent'];
    public $timestamps = false;
}
