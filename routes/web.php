<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'products.index', 'uses' => 'ProductsController@index']);
Route::post('/', ['as' => 'products.search', 'uses' => 'ProductsController@index']);
Route::get('/gamelist', ['as' => 'products.gamelist', 'uses' => 'ProductsController@gamelist']);
Route::get('/blacklist', ['as' => 'products.blacklist', 'uses' => 'ProductsController@blacklist']);
Route::get('/faq', ['as' => 'pages.faq', 'uses' => function() {
    return view('pages.faq');
}]);
Route::get('/contacts', ['as' => 'pages.contacts', 'uses' => function() {
    return view('pages.contacts');
}]);

Route::get('login', ['middleware' => 'admin', 'as' => 'login', 'uses' => 'Admin\Controller@login']);
Route::post('login', ['as' => 'login.post', 'uses' => 'Admin\Controller@auth']);

Route::group(['middleware' => 'admin'], function() {
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function() {
        Route::get('/', ['as' => 'admin.index', 'uses' => 'Controller@index']);
        
        Route::get('users/{id}/delete', ['as' => 'admin.users.delete', 'uses' => 'Controller@userDelete']);
        Route::post('users/store', ['as' => 'admin.users.store', 'uses' => 'Controller@userStore']);
        
        Route::get('products/{id}/unneed', ['as' => 'admin.products.unneed', 'uses' => 'Controller@productUnneed']);
        Route::get('products/unneed', ['as' => 'admin.products.unneed.all', 'uses' => 'Controller@productUnneedAll']);
        Route::post('products/need', ['as' => 'admin.products.need', 'uses' => 'Controller@productsNeed']);
        
        Route::post('products/unneed', ['as' => 'admin.products.unneed.store', 'uses' => 'Controller@productsUnneed']);
        Route::get('products/ununneed', ['as' => 'admin.products.ununneed.all', 'uses' => 'Controller@productsUnunneedAll']);
        Route::get('products/{id}/ununneed', ['as' => 'admin.product.ununneed', 'uses' => 'Controller@productUnunneed']);
        
        Route::get('products/{id}/unindex', ['as' => 'admin.products.unindex', 'uses' => 'Controller@productUnindex']);
        Route::get('products/unindex', ['as' => 'admin.products.unindex.all', 'uses' => 'Controller@productUnindexAll']);
        Route::post('products/index', ['as' => 'admin.products.to_index', 'uses' => 'Controller@productsIndex']);
        
        Route::get('products/{id}/unfix', ['as' => 'admin.products.unfix', 'uses' => 'Controller@productUnfix']);
        Route::get('products/unfix', ['as' => 'admin.products.unfix.all', 'uses' => 'Controller@productUnfixAll']);
        Route::post('products/fix', ['as' => 'admin.products.fix', 'uses' => 'Controller@productsFix']);
        Route::post('products/fix/message', ['as' => 'admin.products.fix.message', 'uses' => 'Controller@productsFixMessage']);

        Route::get('commissions/{id}/delete', ['as' => 'admin.commissions.delete', 'uses' => 'Controller@commissionDelete']);
        Route::get('commissions/delete', ['as' => 'admin.commissions.delete.all', 'uses' => 'Controller@commissionDeleteAll']);
        Route::post('commissions/store', ['as' => 'admin.commissions.store', 'uses' => 'Controller@commissionStore']);
    });
});