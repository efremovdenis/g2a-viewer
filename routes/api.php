<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::middleware('client')->get('/healthcheck', function() {
//     return response()->json(['code' => 'success', 'message' => 'All looks fine!'], 204);
// });
Route::get('oauth/token', [
    'uses' => '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken',
    'as' => 'passport.token',
    'middleware' => 'throttle',
]);