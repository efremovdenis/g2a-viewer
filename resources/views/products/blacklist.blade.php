@extends('layouts.index')

@section('content')
<section class="section">
    <div class="container">
        <h2 class="title">
            <svg class="icon title__icon" aria-hidden="true">
                <use xlink:href="/images/sprite.svg#c-hot"></use>
            </svg>
            <span>Список товаров, которые временно не нужны</span>
        </h2>
        <div class="row d-none">
            <div class="col-xl-7">
                <div class="search search--use-table">
                    <input type="text" class="search__input search__input_small" placeholder="Введите ссылку на товар с G2A">
                    <button class="btn search__close search__close_small" type="button">
                        <svg class="icon search__icon" aria-hidden="true">
                            <use xlink:href="/images/sprite.svg#close"></use>
                        </svg>
                    </button>
                    <button class="btn btn-primary search__submit search__submit_small" type="button">Поиск</button>
                    <div class="search__alert" style="display: none;">
                        <p>Ошибка</p>
                        <button class="btn btn-danger btn_size_large" type="button">
                            <svg class="icon search__icon" aria-hidden="true">
                                <use xlink:href="/images/sprite.svg#tick-inside-circle"></use>
                            </svg>
                            <span>Success</span>
                        </button>
                        <button class="btn alert-close" type="button">
                            <svg class="icon search__icon" aria-hidden="true">
                                <use xlink:href="/images/sprite.svg#close"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <ul class="nav nav-list" id="currency-list">
                    @foreach(config('currencies') as $key => $currency)
                        <li class="nav-list__item">
                            <a class="btn btn-{{ $currency['btn-bg'] }} text-uppercase{{ $key == 'USD' ? ' active' : '' }}">{{ $key }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="table-box">
            <div class="table__filter filter collapse d-none" id="tableFilter">
                <div class="card card-body">
                <div class="row mb-2 filter-category">
                        <div class="col-auto">Категория:</div>
                        @foreach($publishers as $publisher)
                            <div class="col-auto">
                                <label class="checkbox">
                                    <input class="checkbox__input" type="checkbox" name="{{ $publisher }}">
                                    <svg class="icon checkbox__icon" aria-hidden="true">
                                        <use xlink:href="/images/sprite.svg#check"></use>
                                    </svg>
                                    <span>{{ ucfirst($publisher) }}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <p>Платформа:</p>
                    <div class="row filter-platform">
                        @foreach($platforms as $platform)
                            <div class="col-auto">
                                <label class="checkbox">
                                    <input class="checkbox__input" type="checkbox" name="{{ $platform }}">
                                    <svg class="icon checkbox__icon" aria-hidden="true">
                                        <use xlink:href="/images/sprite.svg#check"></use>
                                    </svg>
                                    <span>{{ ucfirst($platform) }}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn filter__close collapsed" type="button" href="#tableFilter" role="button" aria-expanded="false" aria-controls="tableFilter">
                        <svg class="icon filter__icon" aria-hidden="true">
                            <use xlink:href="/images/sprite.svg#close"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-datatable">
                    <thead>
                        <tr>
                            <th scope="col">
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th class="table__title" scope="col">
                                <span>Название товара</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                                <button class="btn btn-secondary ml-1 collapsed table-filter-btn d-none" type="button" data-toggle="collapse" data-target="#tableFilter" aria-expanded="false" aria-controls="tableFilter">Фильтр</button>
                            </th>                         
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            @include('partials.products.blacklist_row', ['product' => $product])
                        @endforeach                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection