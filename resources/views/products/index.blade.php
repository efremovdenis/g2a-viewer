@extends('layouts.index')

@section('content')
<section class="section">
    <div class="container">
        
        <div class="container__inner">
            <form class="search" method="POST" action="{{ route('products.search') }}" autocomplete="off">
                {{ csrf_field() }}
                <input type="text" name="link" class="search__input" placeholder="Введите ссылку на товар с G2A" autocomplete="off">
                <button class="btn search__close" type="button">
                    <svg class="icon search__icon" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#close"></use>
                    </svg>
                </button>
                <button class="btn btn-primary search__submit" type="submit">Поиск</button> 

                @if($product->is_fixed && false)
                    <div class="search__alert alert show" role="alert">
                        <p>{{ App\Setting::find('is_fixed_message')->value }}</p>
                        <button class="btn btn-danger btn_size_large" data-dismiss="alert" type="button">
                            <svg class="icon search__icon" aria-hidden="true">
                                <use xlink:href="/images/sprite.svg#tick-inside-circle"></use>
                            </svg>
                            <span>Закрыть</span>
                        </button>
                        <button class="btn alert-close" data-dismiss="alert" aria-label="Close" type="button">
                            <svg class="icon search__icon" aria-hidden="true">
                                <use xlink:href="/images/sprite.svg#close"></use>
                            </svg>
                        </button>
                    </div>
                @endif               
            </form>


            <ul class="nav nav-list" role="tablist" id="currency-list">
                @foreach(config('currencies') as $key => $data)
                    <li class="nav-list__item" role="presentation">
                        <a class="btn btn-{{ $data['btn-bg'] }} text-uppercase{{ $key == 'USD' ? ' active' : '' }}" id="{{ $key }}-tab" data-toggle="tab" href="#{{ $key }}" role="tab" aria-controls="{{ $key }}" aria-selected="{{ ($key == 'USD') ? 'true' : 'false' }}">{{ $key }}</a>
                    </li>
                @endforeach
            </ul>


            <div class="tab-content">
                @foreach(config('currencies') as $key => $currency)
                    <div class="tab-pane fade {{ ($key == 'USD') ? 'show active' : '' }}" id="{{ $key }}" role="tabpanel" aria-labelledby="{{ $key }}-tab">
                        <div class="row">
                            <div class="col-md-6 mb-3 mb-md-0">
                                <h3>{{ $product->name }}</h3>
                                <ul class="info-list">
                                    <li class="info-list__item info-list__item_theme_skyblue">
                                        <div>{{ $product->is_fixed ? 'Фиксированная цена' : 'Цена товара на G2A' }}</div>
                                        <div class="info-list__price">{{ $product->price('g2a_price', $key) }}{{ $currency['symbol'] }}</div>
                                    </li>
                                    <li class="info-list__item info-list__item_theme_red">
                                        <div>Ваша выгода</div>
                                        <div class="info-list__price">{{ $product->price('your_profit', $key) }}{{ $currency['symbol'] }}</div>
                                    </li>
                                    <li class="info-list__item info-list__item_theme_blue">
                                        <div>Наша выгода</div>
                                        <div class="info-list__price">{{ $product->is_fixed ? 'FIX' : $product->price('our_profit', $key).$currency['symbol'] }}</div>
                                    </li>
                                    <li class="info-list__item info-list__item_theme_yellow">
                                        <div>Комиссия G2A</div>
                                        <div class="info-list__price">{{ $product->is_fixed ? 'FIX' : $product->price('g2a_commission', $key).$currency['symbol'] }}</div>
                                    </li>
                                </ul>
                                <div class="text-center">
                                    <a href="tg://resolve?domain=qwerty123" class="btn btn-info btn_size_large">Связаться через Telegram</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="chart-box">
                                    <canvas class="chart" id="chart-{{ $key }}" width="340" height="340"></canvas>
                                    <div class="chart-box__percentage">{{ $product->commission * 100 }}%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@if(!isset($neededProducts) || $neededProducts->count() > 0)
<section class="section">
    <div class="container">
        <h2 class="title">
            <svg class="icon title__icon" aria-hidden="true">
                <use xlink:href="/images/sprite.svg#c-hot"></use>
            </svg>
            <span>Срочно нужны</span>
        </h2>
        <div class="table-box">
            <div class="table-responsive">
                <table class="table table-datatable" data-page-length='30' data-paging="false" data-order="[[2, &quot;desc&quot;]]">
                    <thead>
                        <tr>
                            <th scope="col">
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th class="table__title" scope="col">
                                <span>Название товара</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Объем</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Ваши Выплата</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Процент</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Средняя цена G2A</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($neededProducts as $row)
                            @include('partials.products.needed_row', ['product' => $row])
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endif
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        let currencies = [{!! implode(',', array_map(function($i) { return '"'.$i.'"';}, array_keys(config('currencies')))) !!}];
        currencies.forEach(function(item){
            new Chart('chart-'+item, {
                type: "pie",
                gridLines: { display: !1 },
                data: {
                    datasets: [
                        {
                            data: [
                                parseFloat({{ $product->price('your_profit', "USD") }}), // red
                                parseFloat({{ $product->price('g2a_commission', "USD") }}), // orange
                                parseFloat({{ $product->price('our_profit', "USD") }}), // blue
                            ],
                            backgroundColor: ["rgb(145, 37, 30)", "rgb(215, 176, 66)", "rgb(34, 72, 172)"],
                            label: "Dataset 1",
                        },
                    ],
                    labels: ["Red", "Orange", "Blue"],
                },
                options: {
                    tooltips: { enabled: !0 },
                    responsive: !0,
                    legend: { display: !1 },
                    elements: { arc: { borderWidth: 0 } },
                },
            });
        });

        let crn = get_cookie('crn');
        
        $("#currency-list").on('click', 'a', function() {
            $(".tab-pane").removeClass('active').removeClass('show');
            let cl = $(this).text();
            $(".tab-pane#"+cl).addClass('active').addClass('show');
            set_cookie("crn", cl);
            crn = cl;
            $("#currency-list a").removeClass('active');
            $(this).addClass('active');
            $("span.currency").removeClass('d-none').addClass("d-none");
            $("span.currency-" + cl).removeClass('d-none');
        });
        if(crn != null) {
            $("#currency-list a:contains("+crn+")").trigger('click');
        } else {
            crn = "USD";
        }
        $(".dataTable").on('draw.dt', function() {
            $("span.currency").removeClass('d-none').addClass("d-none");
            $("span.currency-" + crn).removeClass('d-none');
        });

        $(".btn.search__close").click(function() {
            $(".search__input[name=link]").val("");
        });
    });
</script>
@endpush