<tr>
    <td data-sort="{{ $product->platform }}">
        @include('partials.products.platform', ['platform' => $product->platform])
        <div class="d-none icon-id">{{ $product->g2a_product_id }}</div>
    </td>
    <td class="table__title">
        {{ $product->name }}
        <div class="d-none category">{{ $product->publisher }}</div>
        <div class="d-none platform">{{ $product->platform }}</div>
    </td>
    <td>{{ $product->qty }}</td>
    <td>
        @foreach(config('currencies') as $key => $currency)
            <span class="currency currency-{{ $key }}{{ $key != 'USD' ? ' d-none' : '' }}">{{ $product->price('your_profit', $key) }} {{ $key }}</span>
        @endforeach
    </td>
    <td>{{ $product->commission * 100}}%</td>
    <td>
        @foreach(config('currencies') as $key => $currency)
            <span class="currency currency-{{ $key }}{{ $key != 'USD' ? ' d-none' : '' }}">{{ $product->price('g2a_price', $key) }} {{ $key }}</span>
        @endforeach
    </td>
</tr>