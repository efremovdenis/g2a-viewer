<tr>
    <td data-sort="{{ $product->platform }}">
        @include('partials.products.platform', ['platform' => $product->platform])
        <div class="d-none icon-id">{{ $product->g2a_product_id }}</div>
    </td>
    <td class="table__title">
        {{ $product->name }}
        <div class="d-none category">{{ $product->publisher }}</div>
        <div class="d-none platform">{{ $product->platform }}</div>
    </td>
</tr>