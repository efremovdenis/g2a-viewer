@switch(mb_strtolower($platform))
    @case("playstation")
        <img src="/images/platforms/sony_p.svg" width="24" height="24" />
    @break
    @case("xbox live")
        <img src="/images/platforms/xbox.svg" width="24" height="24" />
    @break
    @case("google play")
        <img src="/images/platforms/googleplay.svg" width="24" height="24" />
    @break
    @case("apple itunes")
        <img src="/images/platforms/itunes.svg" width="24" height="24" />
    @break
    @case("origin")
        <img src="/images/platforms/origin.svg" width="24" height="24" />
    @break
    @case("blizzard")
        <img src="/images/platforms/blizzard.svg" width="24" height="24" />
    @break
    @case("windows")
        <img src="/images/platforms/microsoft.svg" width="24" height="24" />
    @break
    @case("nintendo")
        <img src="/images/platforms/nintendo.svg" width="24" height="24" />
    @break
    @case("steam")
        <img src="/images/platforms/steam.svg" width="24" height="24" />
    @break
    @case("uplay")
        <img src="/images/platforms/uplay.svg" width="24" height="24" />
    @break
    @case("xbox")
        <img src="/images/platforms/xbox.svg" width="24" height="24" />
    @break
    @case("netflix")
        <img src="/images/platforms/netflix.svg" width="24" height="24" />
    @break
    @case("rockstar social club")
        <img src="/images/platforms/rsc.svg" width="24" height="24" />
    @break
    @case("rockstar")
        <img src="/images/platforms/rockstar.svg" width="24" height="24" />
    @break
    @case("GOG Ltd")
        <img src="/images/platforms/gog.svg" width="24" height="24" />
    @break
    @case("amazon")
        <img src="/images/platforms/amazon.svg" width="24" height="24" />
    @break
    @case("bethesda")
        <img src="/images/platforms/bethesda.svg" width="24" height="24" />
    @break
    @case("software")
        <img src="/images/platforms/software.svg" width="24" height="24" />
    @break
    @case("epic games")
        <img src="/images/platforms/epicgames.svg" width="24" height="24" />
    @break
    @default
        <img src="/images/platforms/another.svg" width="24" height="24" />
    @break
@endswitch