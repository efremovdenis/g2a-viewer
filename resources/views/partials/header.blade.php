<header class="header">
    <div class="container">
        <nav class="navbar navbar-expand-sm">
            <a href="{{ route('products.index') }}" class="navbar-brand">
                <img src="/images/logo.png" alt="Logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <svg class="icon navbar-toggler-icon" aria-hidden="true">
                    <use xlink:href="/images/sprite.svg#menu"></use>
                </svg>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('products.gamelist') }}">Список товаров</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('products.blacklist') }}">Ограниченный список</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('pages.faq') }}">FAQ</a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Отзывы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('pages.contacts') }}">Контакты</a>
                    </li>
                    @if(auth()->check())
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.index') }}">ADMIN</a>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>
        @if(!isset($do_not_show_slogan) || !$do_not_show_slogan)
        <p class="slogan">Мы купим ваши ключи, е-гифты, подписки дороже других</p>
        @endif
    </div>
</header>