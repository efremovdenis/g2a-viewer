@if(Session::has('system') && Session::get('system') == 'product_not_found')
    <div class="alert alert-danger" role="alert" style="width: 50vw; left: 25%; position: absolute; z-index: 2;">
        {{ App\Setting::find('product_not_found')->value }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(Session::has('danger'))
    <div class="alert alert-danger" role="alert" style="width: 50vw; left: 25%; position: absolute; z-index: 2;">
        {{ Session::get('danger') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(Session::has('success'))
    <div class="alert alert-success" role="alert" style="width: 50vw; left: 25%; position: absolute; z-index: 2;">
        {{ Session::get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(isset($errors) && $errors->any()) 
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert" style="width: 50vw; left: 25%; position: absolute; z-index: 2;">
            {{ $error }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endforeach
@endif
