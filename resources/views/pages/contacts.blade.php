@extends('layouts.index', ['title' => 'TEST'])

@section('content')
<section class="section">
    <div class="container">
        <h2 class="title">
        <svg class="icon title__icon" aria-hidden="true">
            <use xlink:href="./images/sprite.svg#c-hot"></use>
        </svg>
        <span>Контакты</span>
        </h2>

        <div class="info">
        <br />
        <p>По всем вопросам пишите на почту: support@site.com</p>
        <br />
        </div>
    </div>
    </section>
@endsection