@extends('layouts.index', ['do_not_show_slogan' => true])

@section('content')
<section class="section">
	<div class="container">
		<h2 class="title">
			<svg class="icon title__icon" aria-hidden="true">
				<use xlink:href="/images/sprite.svg#c-hot"></use>
			</svg>
			<span>Вход в панель</span>
		</h2>

		<div class="admin">
            <form action="{{ route('login.post') }}" method="POST">
                {{ csrf_field() }}
                <div class="row justify-content-center">
                    <div class="form-group col-4">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" />
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="form-group col-4">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" />
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="form-group col-4 text-center">
                        <input type="submit" class="btn btn-success p-1 px-5" value="Auth" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection