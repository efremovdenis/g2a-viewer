@extends('layouts.index', ['do_not_show_slogan' => true])


@section('content')
<section class="section">
	<div class="container">
		<h2 class="title">
			<svg class="icon title__icon" aria-hidden="true">
				<use xlink:href="/images/sprite.svg#c-hot"></use>
			</svg>
			<span>Админка</span>
		</h2>

		<ul class="nav nav-list mb-2 d-none" id="currency-list">
			@foreach(config('currencies') as $key => $currency)
				<li class="nav-list__item">
					<a class="btn btn-{{ $currency['btn-bg'] }} text-uppercase">{{ $key }}</a>
				</li>
			@endforeach
		</ul>

		<div class="admin">
			<ul class="nav admin__nav" role="tablist">
				<li class="nav-item" role="presentation">
					<a class="nav-link active" id="statistics-tab" data-toggle="tab" href="#statistics" role="tab" aria-controls="statistics" aria-selected="true">Статистика</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="fixed-goods-tab" data-toggle="tab" href="#fixed-goods" role="tab" aria-controls="fixed-goods" aria-selected="false">Фисксированные товары</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="urgently-needed-tab" data-toggle="tab" href="#urgently-needed" role="tab" aria-controls="urgently-needed" aria-selected="false">Срочно нужны</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="products-index-tab" data-toggle="tab" href="#products-index" role="tab" aria-controls="products-index" aria-selected="false">Рандомные на главной</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="exceptions-tab" data-toggle="tab" href="#exceptions" role="tab" aria-controls="exceptions" aria-selected="false">Исключения</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="false">Пользователи</a>
				</li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane fade show active" id="statistics" role="tabpanel" aria-labelledby="statistics-tab">
					@include('admin.tabs.commissions')
				</div>

				<div class="tab-pane fade" id="fixed-goods" role="tabpanel" aria-labelledby="fixed-goods-tab">
					@include('admin.tabs.fixed')
				</div>

				<div class="tab-pane fade" id="urgently-needed" role="tabpanel" aria-labelledby="urgently-needed-tab">
					@include('admin.tabs.needed')
				</div>

				<div class="tab-pane fade" id="products-index" role="tabpanel" aria-labelledby="products-index-tab">
					@include('admin.tabs.index')
				</div>

				<div class="tab-pane fade" id="exceptions" role="tabpanel" aria-labelledby="exceptions-tab">
					@include('admin.tabs.excepted')
				</div>
				<div class="tab-pane fade" id="users" role="tabpanel" aria-labelledby="users-tab">
					@include('admin.tabs.users')					
				</div>
			</div>
		</div>
	</div>
</section>
@endsection