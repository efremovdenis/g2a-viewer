<h3 class="statistics__title">Пользователи системы</h3>
<form class="statistics__controls" action="{{ route('admin.users.store') }}" method="POST" style="width:auto">
    {{ csrf_field() }}
    <div>
        <label class="statistics__label">
            <input class="statistics__input" style="width: 180px" type="text" name="name" />
            <span class="statistics__subtext">Name</span>
        </label>
        <label class="statistics__label">
            <input class="statistics__input" style="width: 180px" type="text" name="email" />
            <span class="statistics__subtext">Email</span>
        </label>
        <label class="statistics__label">
            <input class="statistics__input" style="width: 180px" type="text" name="password" />
            <span class="statistics__subtext">Password</span>
        </label>
        <label class="statistics__label">
            <input class="statistics__input" style="width: 180px" type="text" name="public_key" />
            <span class="statistics__subtext">Client Public</span>
        </label>
        <label class="statistics__label">
            <input class="statistics__input" style="width: 180px" type="text" name="secret_key" />
            <span class="statistics__subtext">Client Secret</span>
        </label>
        <button class="btn btn-primary statistics__button" type="submit">
            Добавить
        </button>
    </div>
</form>

<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th class="table__title" scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Client Public</th>
                <th scope="col">Client Secret</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($users->where('name', '!=', 'root') as $user)
                <tr>
                    <td class="table__title">{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->public_key }}</td>
                    <td>{{ $user->secret_key }}</td>
                    <td><a href="{{ route('admin.users.delete', $user->id) }}">Удалить</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>