<div class="row mb-5">
    <div class="col-auto">
        <form class="mb-5" method="POST" action="{{ route('admin.products.fix') }}">
            {{ csrf_field() }}
            <textarea class="info__area" id="id-list" placeholder="Список ID..." name="ids"></textarea>
            <div class="info__controls">
                <button class="btn btn-primary" type="submit">
                    Добавить
                </button>
            </div>
        </form>

        <form method="POST" action="{{ route('admin.products.fix.message') }}">
            {{ csrf_field() }}
            <textarea class="info__area info__area_small" id="warning-id-message" name="message" placeholder="Ваше предупреждение...">{{ App\Setting::find('is_fixed_message')->value }}</textarea>
            <div class="info__controls">
                <button class="btn btn-primary" type="submit">
                    Сохранить
                </button>
            </div>
        </form>
    </div>
</div>

<div class="d-flex mb-3">
    <a class="btn btn-primary admin__button ml-auto" href="{{ route('admin.products.unfix.all') }}">
        Удалить все
    </a>
</div>

<div class="table-responsive">
    <table class="table table-datatable">
        <thead>
            <tr>
                <th class="table__title" scope="col">
                    <span>ID товара</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th class="table__title" scope="col">
                    <span>Название товара</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th scope="col">
                    <span>Выплата</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th scope="col">
                    <span>Действие</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($products->where('is_fixed', true) as $product)
                <tr>
                    <td class="table__title">{{ $product->g2a_product_id }}</td>
                    <td class="table__title">{{ $product->name }}</td>
                    <td>{{ $product->price }} USD</td>
                    <td>
                        <a href="{{ route('admin.products.unfix', $product->g2a_product_id) }}">Снять фиксацию</a>
                    </td>
                </tr>
            @endforeach            
        </tbody>
    </table>
</div>