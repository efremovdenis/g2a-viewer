<h3 class="statistics__title">Формула комиссии</h3>
<form class="statistics__controls" action="{{ route('admin.commissions.store') }}" method="POST">
    {{ csrf_field() }}
    <div class="statistics__range mb-3 mb-sm-0">
        <label class="statistics__label">
            <input class="statistics__input" type="text" name="min_limit" />
            <span class="statistics__subtext">От</span>
        </label>
        <span class="statistics__divider">-</span>
        <label class="statistics__label">
            <input class="statistics__input" type="text" name="max_limit" />
            <span class="statistics__subtext">До</span>
        </label>
    </div>
    <div class="ml-auto">
        <label class="statistics__label">
            <input class="statistics__input" type="text" name="percent" />
            <span class="statistics__subtext">Процент</span>
        </label>
        <button class="btn btn-primary statistics__button" type="submit">
            Добавить
        </button>
    </div>
</form>

<div class="d-flex mb-3">
    <a class="btn btn-primary admin__button ml-auto" href="{{ route('admin.commissions.delete.all') }}">
        Удалить все
    </a>
</div>

<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th class="table__title" scope="col">
                    Категория цены
                </th>
                <th scope="col">Процент для формулы</th>
                <th scope="col">Действие</th>
                <th scope="col" class="table__blank"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($commissions as $commissiion)
                <tr>
                    <td class="table__title">
                        От {{ $commissiion->min_limit }} до {{ $commissiion->max_limit }} USD
                    </td>
                    <td>{{ $commissiion->percent }}</td>
                    <td><a href="{{ route('admin.commissions.delete', $commissiion->id) }}">Удалить</a></td>
                    <td class="table__blank"></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>