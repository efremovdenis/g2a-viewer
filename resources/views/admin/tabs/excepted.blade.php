<div class="row mb-5">
    <div class="col-auto">
        <form class="mb-5" method="POST" action="{{ route('admin.products.unneed.store') }}">
            {{ csrf_field() }}
            <textarea class="info__area" id="id-list" placeholder="Список ID..." name="ids">@foreach($products->where('is_not_needed_force', true)->all() as $product) {{ $product->g2a_product_id }}@endforeach</textarea>
            <div class="info__controls">
                <button class="btn btn-primary" type="submit">
                    Добавить
                </button>
            </div>
        </form>
    </div>
</div>

<div class="d-flex mb-3">
    <a class="btn btn-primary admin__button ml-auto" href="{{ route('admin.products.ununneed.all') }}">
        Удалить все
    </a>
</div>
<div class="table-responsive">
    <table class="table table-datatable">
        <thead>
            <tr>
                <th class="table__title" scope="col">
                    <span>ID товара</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th class="table__title" scope="col">
                    <span>Название товара</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th scope="col">
                    <span>Выплата</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th scope="col">
                    <span>Требуемое количество</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th scope="col">
                    <span>Продаваемое количество</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
                <th scope="col">
                    <span>Дейсвие</span>
                    <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#up-arrow"></use>
                    </svg>
                    <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                        <use xlink:href="/images/sprite.svg#down-arrow"></use>
                    </svg>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($products->where('is_not_needed_force', true) as $product)
                <tr>
                    <td class="table__title">{{ $product->g2a_product_id }}</td>
                    <td class="table__title">{{ $product->name }}</td>
                    <td>{{ $product->price }} USD</td>
                    <td>{{ $product->qty }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td><a href="{{ route('admin.product.ununneed', $product->g2a_product_id) }}">Снять FORCE unneed</a></td>
                </tr>
            @endforeach            
        </tbody>
    </table>
</div>