@extends('layouts.index')

@section('content')
<section class="section mb-5">
	<div class="container">
		<div class="container__inner text-center">
			<img src="/images/404.png" alt="404" />
		</div>
	</div>
</section>
@if(isset($neededProducts))
<section class="section">
	<div class="container">
		<h2 class="title">
			<svg class="icon title__icon" aria-hidden="true">
				<use xlink:href="/images/sprite.svg#c-hot"></use>
			</svg>
			<span>Срочно нужны</span>
		</h2>
		<div class="table-box">
			<div class="table-responsive">
				<table class="table table-datatable-404" data-page-length='20'>
					<thead>
                        <tr>
                            <th scope="col">
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th class="table__title" scope="col">
                                <span>Название товара</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Объем</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Ваши Выплата</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Процент</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                            <th scope="col">
                                <span>Средняя цена G2A</span>
                                <svg class="icon table__icon icon-up" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#up-arrow"></use>
                                </svg>
                                <svg class="icon table__icon icon-down" width="14" height="14" aria-hidden="true">
                                    <use xlink:href="/images/sprite.svg#down-arrow"></use>
                                </svg>
                            </th>
                        </tr>
                    </thead>
					<tbody>
						@foreach($neededProducts as $row)
                            @include('partials.products.needed_row', ['product' => $row])
                        @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
@endif
@endsection