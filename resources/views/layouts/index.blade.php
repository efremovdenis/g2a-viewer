<!doctype html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? env('APP_NAME') }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
    <link href="/styles/vendors.47801551eb17c453bfd3.css" rel="stylesheet">
    <link href="/styles/app.47801551eb17c453bfd3.css" rel="stylesheet">
</head>

<body>

    <div class="page">
        <div>
            @include('partials.header')
            @include('partials.alerts')
            
            <main class="main">
                @yield('content')
            </main>

            <div class="contacts">
                <address>
                    <div>Контактные данные:</div>
                    <div>Telegram: <a href="tg://resolve?domain=@qwerty123">@qwerty123</a></div>
                </address>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <address>
                    <div>Контактные данные:</div>
                    <div>Telegram: <a href="tg://resolve?domain=@qwerty123">@qwerty123</a></div>
                </address>
            </div>
        </footer>
    </div>

    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>

    <script type="text/javascript" src="/scripts/vendors.47801551eb17c453bfd3.js"></script>
    <script type="text/javascript" src="/scripts/app.47801551eb17c453bfd3.js"></script>
    <script type="text/javascript" src="/scripts/cookie.js"></script>
    @stack('scripts')
</body>

</html>